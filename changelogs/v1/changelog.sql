--liquibase formatted sql

--changeset sergio:1
create  table  scientists  (
    id  serial  primary  key,
    name  varchar(255) not null
);
--rollback drop table scientists;

--changeset sergio:2
insert  into  scientists  (name)  values  ('Newton');
insert  into  scientists  (name)  values  ('Leibniz');
--rollback truncate table scientists;

--changeset sergio:3
insert  into  scientists  (name)  values  ('Faraday');
--rollback delete from scientists where name='Faraday';

--changeset sergio:4
update  scientists  set  name='Maxwell'  where  name='Faraday';
--rollback update  scientists  set  name='Faraday'  where  name='Maxwell';

--changeset sergio:5
insert  into  scientists  (name)  values  ('Einstein');
insert  into  scientists  (name)  values  ('Curie');
--rollback delete from scientists where name in ('Einstein', 'Curie');
